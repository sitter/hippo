// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2
import org.kde.kirigami 2.12 as Kirigami
import org.kde.hippo 1.0

Kirigami.ApplicationWindow {
    id: appWindow

    title: i18nc("@title:window", "🦛 Hippo 🦛")
    minimumWidth: Kirigami.Units.gridUnit * 22
    minimumHeight: Kirigami.Units.gridUnit * 22

    // TODO: maybe control on pages individually. this is here cause otherwise the ItemPage would have empty title bars
    pageStack.globalToolBar.style: Kirigami.ApplicationHeaderStyle.None

    function replaceItemPage(properties) {
        pageStack.clear()
        pageStack.replace("qrc:/ui/ItemPage.qml", properties)
    }

    footer: QQC2.ScrollBar {
        // Hack a toolbar in by making it the footer. A bit glitchy and and really not ideal but good enough for now.
        // Courtesy of
        // SPDX-FileCopyrightText: 2016 Marco Martin <mart@kde.org>
        // SPDX-License-Identifier: LGPL-2.0-or-later
        orientation: Qt.Horizontal
        position: appWindow.pageStack.columnView.contentX/appWindow.pageStack.columnView.contentWidth
        onPositionChanged: appWindow.pageStack.columnView.contentX = position * appWindow.pageStack.columnView.contentWidth
        size: appWindow.pageStack.columnView.width/appWindow.pageStack.columnView.contentWidth
    }

    // TODO: ponder the inner workings of the places bar. currently this is a navigation bar
    //  so the current entry is always the "root" of the path you are browsing. that is a bit
    //  limiting though. it is also removing the notion of writing paths manually though...
    //  so
    //  that
    //  is
    //  an
    //  advantage
    globalDrawer: Kirigami.GlobalDrawer {
        modal: !wideScreen
        handleVisible: !wideScreen

        leftPadding: 0
        rightPadding: 0
        topPadding: 0
        bottomPadding: 0

        // Too fat by default.
        width: Kirigami.Units.gridUnit * 10

        ListView {
            id: view
            Layout.fillWidth: true
            Layout.fillHeight: true
            model: PlacesModel{}
            currentIndex: 0
            delegate: Kirigami.BasicListItem {
                id: item
                // FIXME: this needs changing
                //   - it looks horrible
                //   - it bugs out (because of BasicListItem I think) where you can
                //     move one item under the other without triggering the move, effectively
                //     losing the item
                Kirigami.ListItemDragHandle {
                    listItem: item
                    listView: view
                    onMoveRequested: view.model.move(oldIndex, newIndex)
                }

                // Hack to separte the drawer visually from the itempages
                backgroundColor: Kirigami.Theme.backgroundColor
                Kirigami.Theme.inherit: false
                Kirigami.Theme.colorSet: Kirigami.Theme.Window

                icon: model.IconNameRole
                label: model.display
                action: Kirigami.Action {
                    onTriggered: {
                        console.log("replace %1".arg(model.UrlRole))
                        replaceItemPage({dir: model.UrlRole})
                    }
                }
                Component.onCompleted: {
                    // FIXME: bit of a stop-gap decide if worth keeping. first entry of placs is always default view
                    if (view.currentIndex == index) {
                        action.trigger()
                    }
                }
            }

        moveDisplaced: Transition {
            YAnimator {
                duration: Kirigami.Units.longDuration
                easing.type: Easing.InOutQuad
            }
        }

            section.property: "GroupName"
            section.delegate: Kirigami.ListSectionHeader {
                label: section
            }
        }
    }
}
