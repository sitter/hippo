// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2
import org.kde.kirigami 2.12 as Kirigami
import org.kde.hippo 1.0
import org.kde.kitemmodels 1.0 as KItemModels

// /home/me/src/plasma-desktop/containments/desktop/plugins/folder
import org.kde.private.desktopcontainment.folder 0.1 as Folder

// containments/desktop/package/contents/ui/FolderView.qml

Kirigami.ScrollablePage {
    id: page
    property alias dir: dirModel.url
    property bool shifty: false
    property int shiftyAnchor: -1

    onActiveFocusChanged: {
        console.log("%1; title=%2".arg(this).arg(basename(dir)))
        console.log(activeFocus)
    }
    Kirigami.ColumnView.fillWidth: false
    // When the current page isn't active switch the color profile. This ensures
    // that it is obvious which column currently has focus and through selection inside the view it's obvious
    // what the directoy path is.
    Kirigami.Theme.colorGroup: pageStack.currentItem === this ? Kirigami.Theme.Active : Kirigami.Theme.Inactive

    function basename(str) {
        return (str.slice(str.lastIndexOf("/") + 1))
    }

    title: "((TODO))" // do we even want a title? currently the titlebar is entirely disabled in the main.qml
    Folder.FolderModel {
        id: dirModel
    }

    KItemModels.KSortFilterProxyModel {
        id: filteredModel
        sourceModel: dirModel
        filterRole: "display"
        // FIXME there's no way to do case insensitive compare :(
        // there's a callback but I really don't want to route 100k rows through js string comparision
    }

    // FIXME wonky hack. somehow setting an id does nothing :shrug:
    property Item filterField

    // TODO: support refreshing?

    ListView {
        id: view

        header: ColumnLayout {
            width: parent.width
            Kirigami.SearchField {
                Component.onCompleted: filterField = this
                visible: false

                placeholderText: i18nc("@label placeholder text to filter for something", "Filter…")

                Accessible.name: i18nc("accessible name for filter input", "Filter")
                Accessible.searchEdit: true

                // NB: the focus sequence is intentionally managed differently because the field is shown on-demand
                Layout.fillWidth: true

                onAccepted: {
                    // TODO: we clear selection because highlight changes mess with focus (Due to another hack!)
                    dirModel.clearSelection()
                    filteredModel.filterString = text
                }
            }
        }

        // reuse delegates so we can deal with huge amounts of entries
        // do not keep state data inside the delegate!
        reuseItems: true

        // Gradually increase the cache buffer up to N pages worth of content.
        // The purpose of this is to keep the view always as responsive as possible.
        // If we were to bump the cache all at once we'd have to load all new delegates
        // all at once. By graudally increasing the cache we space out the work.
        // This adds towards responsiveness in addition to reuseItems. That is why
        // we use only a few pages as cache, the rest of the performance ought to be
        // obtained from recycling items.
        Binding {
            id: cacheBinding
            property int pageCount: 1
            delayed: true
            target: view
            property: 'cacheBuffer'
            value: page.height * pageCount
        }
        Timer {
            interval: 100
            // TODO caching could be subject to config or maybe dynamic increased?
            // if the user doesn't scroll they'll not need much caching. if they
            // start scrolling a lot we could bump the cache up dynamically
            running: cacheBinding.pageCount < 5
            repeat: true
            onTriggered: {
                cacheBinding.pageCount += 1
            }
        }

        currentIndex: -1
        model: filteredModel
        onCurrentItemChanged: {
            console.log("item: %1; title=%2".arg(currentItem).arg(basename(dir)))
        }

        onCurrentIndexChanged: {
            console.log("index: %1; title=%2".arg(currentIndex).arg(basename(dir)))
            if (shifty) {
                dirModel.setRangeSelected(shiftyAnchor, currentIndex)
                return
            }
            dirModel.clearSelection()
            dirModel.toggleSelected(currentIndex)
        }

        // TODO: make delegate as cheap as possible to construct!
        delegate: Kirigami.SwipeListItem {
            // FIXME hack for testing
            // highlighted: currentItem == this
            highlighted: model.selected
            onHighlightedChanged: {
                console.log("highlight: %1; title=%2".arg(this).arg(basename(dir)))
                // FIXME: this is all wrong. like soooooo wrong
                if (highlighted) {
                    pageStack.push("qrc:/ui/InfoPage.qml", {
                        url: model.url,
                        icon: model.decoration,
                        name: model.display
                    })
                }
                // FIXME: also very terrible + this messes with searchfield focus
                page.forceActiveFocus()
                page.focus = true
            }
            checked: view.currentIndex == index

            function openContextMenu() {
                // TODO why is the model in charge of context menus. totally unrelated task to modeling...
                dirModel.openContextMenu(parent)
            }

            property bool isDir: model.isDir
            // Needed so we can have the mouse area span the entire item
            // TODO: an argument could be made that interactability in particular with regards to left mouse button
            //   should be more selective. If I drag from an empty space (even when within a delegate) that should
            //   start a selection not activate it. this is unrelated to the mouse area here tho cause
            //   that is for context actions ;)
            // FIXME: this breaks elide
            contentItem: Item {
                RowLayout {
                    id: layout
                    Kirigami.Icon {
                        source: model.decoration
                    }
                    QQC2.Label {
                        Layout.fillWidth: true
                        text: model.display
                        elide: Text.ElideMiddle
                    }
                }

                MouseArea {
                    z: 100 // take presedence over directory-level context
                    anchors.fill: parent
                    // TODO Could also have an action toolbar for actions so one doesn't have to contextmenu
                    acceptedButtons: Qt.RightButton
                    enabled: true
                    onClicked: openContextMenu()
                }
            }

            action: Kirigami.Action {
                id: openAction
                visible: isDir
                icon.name: "arrow-right"
                // TODO text for accessibility
                onTriggered: {
                    if (model.isDir) {
                        Kirigami.Theme.colorGroup = Kirigami.Theme.Inactive
                        // FIXME code copy from arrow action
                        view.currentIndex = index // FIXME hack to ensure current item is selected?
                        var url = model.url
                        console.log(url)
                        var nextPage = pageStack.items[pageStack.currentIndex + 1]
                        if (nextPage && nextPage.dir == url) {
                            pageStack.goForward()
                            return
                        }
                        console.log("push")
                        pageStack.push("qrc:/ui/ItemPage.qml", {"dir": url })
                    } else {
                        dirModel.runSelected()
                    }
                }
            }

            // For the interactive action on the swipe item
            actions: [action]
        }

        Kirigami.PlaceholderMessage {
            anchors.centerIn: parent
            width: parent.width - (Kirigami.Units.largeSpacing * 4)
            visible: view.count == 0
            text: {
                switch (dirModel.status) {
                case Folder.FolderModel.Listing:
                    return i18nc("@info directory listing in progress", "Loading...")
                case Folder.FolderModel.Canceled:
                    return i18nc("@info directory listing in progress", "Loading Canceld")
                }
                return i18nc("@info view is empty because directory is empty", "Empty")
            }
        }

        MouseArea {
            anchors.fill: parent
            z: -100 // way below the delegate (delegate takes presedence)
            // TODO Could also have an action toolbar for actions so one doesn't have to contextmenu
            acceptedButtons: Qt.RightButton
            enabled: true
            // propagateComposedEvents: true
            onClicked: {
                contextMenu.popup()
                // mouse.accepted = false
            }

            QQC2.Menu {
                // FIXME: the folderModel doesn't actually have a way to contextmenu the "current" dir
                //   as none of the plasma UIs have any such concept :(
                id: contextMenu
                Kirigami.Action {
                    iconName: "edit-paste"
                    text: i18nc("@action:inmenu", "Paste")
                    onTriggered: Clipboard.paste(dir)
                }
                Kirigami.Action {
                    iconName: "document-properties"
                    text: i18nc("@action:inmenu", "Properties")
                    onTriggered: PropertiesDialogFactory.show(dir)
                }
            }
        }
    }

    // Shortcuts are focus dependent because we have multiple pages with the same shortcuts
    Shortcut {
        enabled: page.focus
        sequence: "Ctrl+I"
        onActivated: {
            console.log("---> %1 [%2]".arg(this).arg(dir))
            // TODO: what I would like is for the field to be hidden by default and visible when used and then disappear if empty and unused for a while
            //   but currently the page doesn't reshuffle properly when we add stuff to the header :|
            // if (filterField.visible) {
                // filterField.visible = false
                // return
            // }
            filterField.visible = true
            filterField.focus = true
        }
    }

    Shortcut {
        enabled: true // ambiguously refresh all pages
        sequence: StandardKey.Refresh
        onActivatedAmbiguously: dirModel.refresh()
    }

    // FIXME: selection gets messed up when deleting an item (the item after is highlighted but not selected QQ)
    Shortcut {
        enabled: page.focus
        sequence: StandardKey.Delete
        onActivated: {
            if (dirModel.hasSelection()) {
                dirModel.deleteSelected()
            }
        }
    }

    Shortcut {
        enabled: page.focus
        sequence: StandardKey.Copy
        onActivated: dirModel.copy()
    }

    Shortcut {
        enabled: page.focus
        sequence: StandardKey.Paste
        onActivated: dirModel.paste()
    }

    Shortcut {
        enabled: page.focus
        sequence: StandardKey.Cut
        onActivated: dirModel.cut()
    }

    Shortcut {
        enabled: page.focus
        sequence: StandardKey.Undo
        onActivated: dirModel.undo()
    }

    Shortcut {
        enabled: page.focus
        sequence: StandardKey.MoveToStartOfLine
        onActivated: view.positionViewAtBeginning()
    }

    Shortcut {
        enabled: page.focus
        sequence: StandardKey.MoveToEndOfLine
        onActivated: view.positionViewAtEnd()
    }

    Shortcut {
        enabled: page.focus
        sequence: StandardKey.MoveToPreviousPage
        onActivated: {
            console.log("-- up --")
            let top = view.indexAt(view.contentX, view.contentY)
            let bottom = view.indexAt(view.contentX, view.contentY + view.height)
            if (bottom == -1) {
                bottom = view.count - 1
            }
            let stepSize = bottom - top
            let newIndex = Math.max(top - stepSize, 0)
            console.log("bottom %1 stepSize %2 top %3 jumpto %4".arg(bottom).arg(stepSize).arg(top).arg(newIndex))
            view.positionViewAtIndex(newIndex, ListView.Beginning)
        }
    }

    Shortcut {
        enabled: page.focus
        sequence: StandardKey.MoveToNextPage
        onActivated: {
            console.log("-- down --")
            let top = view.indexAt(view.contentX, view.contentY)
            let bottom = view.indexAt(view.contentX, view.contentY + view.height)
            if (bottom == -1) {
                bottom = view.count - 1
            }
            let stepSize = bottom - top
            let newIndex = Math.min(bottom + stepSize, view.count - 1)
            console.log("bottom %1 stepSize %2 top %3 jumpto %4".arg(bottom).arg(stepSize).arg(top).arg(newIndex))
            view.positionViewAtIndex(newIndex, ListView.End)
        }
    }

    Keys.onPressed: {
        console.log("- key ------- key: %1 text: %2 accepted: %3".arg(event.key).arg(event.text).arg(event.accepted))

        event.accepted = false
        // TODO: surely we have a framework to manage variations of return/enter whatever
        switch (event.key) {
        case Qt.Key_Right:
        case Qt.Key_Return:
            console.log("right")
            console.log(view.currentItem)
            if (view.currentItem) {
                view.currentItem.action.trigger()
            } else {
                pageStack.goForward()
            }
            event.accepted = true
            return
        case Qt.Key_Left:
            console.log("left")
            pageStack.goBack()
            event.accepted = true
            return
        case Qt.Key_Menu:
            if (view.currentItem) {
                // FIXME: this actually positions the menu incorrectly below the mouse rather than near the entry
                view.currentItem.openContextMenu()
            }
            event.accepted = true
            return
        case Qt.Key_Shift:
            shifty = true
            shiftyAnchor = view.currentIndex
            return // NB: do not acceept modifiers - they need to go to possible input fields
        }
    }

    Keys.onReleased: {
        event.accepted = false
        // TODO: surely we have a framework to manage variations of return/enter whatever
        switch (event.key) {
        case Qt.Key_Shift:
            shifty = false
            // Do not reset the shiftyAnchor when shift is released. Otherwise you can't continue
            // selecting.
            // TODO does this actually make sense? in a way I feel like when you start a new selection
            //  set it should start from scratch :shrug:
            // NB: do not acceept modifiers - they need to go to possible input fields
            break
        }
    }
}
