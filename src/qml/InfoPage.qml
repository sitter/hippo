// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>

import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as QQC2
import org.kde.kirigami 2.12 as Kirigami
import org.kde.hippo 1.0

Kirigami.ScrollablePage {
    id: page
    objectName: "InfoPage"
    Kirigami.ColumnView.fillWidth: false
    // TODO: check if ColumnView.pinned might help us. we could have a single page and pin it?

    property variant icon
    property alias url: preview.url
    property alias name: nameHeader.text

    // TODO: find a better way. this is a fairly awkward hack to ensure infopages are
    //  only ever ephemeral. if any other page (could be another infopage, mind) is pushed,
    //  this page removes itself from the stack again (thereby forcing only ever one infopage to be
    //  on the stack and only ever able to be the last page)
    Connections {
        target: pageStack
        function onLastItemChanged() {
            if (pageStack.lastItem !== page) {
                pageStack.removePage(page)
            }
        }
    }

    Preview {
        id: preview
    }

    ColumnLayout {
        Kirigami.Icon {
            Layout.alignment: Qt.AlignHCenter
            // TODO what's the best size. actually. it kinda should be scaling. but oof
            implicitWidth: Kirigami.Units.iconSizes.enormous * 2
            implicitHeight: implicitWidth
            source: preview.hasData ?  preview.data : icon
        }
        Kirigami.Heading {
            id: nameHeader
            Layout.fillWidth: true
            wrapMode: Text.Wrap
            horizontalAlignment: TextEdit.AlignHCenter
        }
    }
}
