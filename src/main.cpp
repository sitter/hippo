// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>

#include <KAuthAction>
#include <KAuthExecuteJob>
#include <KDeclarative/KDeclarative>
#include <KDirLister>
#include <KDirModel>
#include <KDirSortFilterProxyModel>
#include <KFilePlacesModel>
#include <KIO/Paste>
#include <KIO/PasteJob>
#include <KIO/PreviewJob>
#include <KJob>
#include <KLocalizedContext>
#include <KLocalizedString>
#include <KOSRelease>
#include <KPropertiesDialog>
#include <KUrlMimeData>
#include <QApplication>
#include <QClipboard>
#include <QFile>
#include <QIcon>
#include <QMimeData>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QStandardPaths>
#include <QWindow>
#include <QTimer>

#include <chrono>

using namespace std::chrono_literals;

class Preview : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QPixmap data MEMBER m_data NOTIFY changed)
    Q_PROPERTY(bool hasData READ hasData NOTIFY changed)
    Q_PROPERTY(QUrl url READ url WRITE setUrl NOTIFY urlChanged)
public:
    using QObject::QObject;
    ~Preview() override
    {
        maybeKill();
    }

    QUrl url() const
    {
        return m_url;
    }
    void setUrl(const QUrl &url)
    {
        qDebug() << "setting url " << url;
        m_url = url;
        Q_EMIT urlChanged();

        m_timer.disconnect();

        static const auto plugins = KIO::PreviewJob::availablePlugins();
        m_job = KIO::filePreview({m_url}, QSize(256, 256), &plugins);
        connect(m_job, &KIO::PreviewJob::result, this, [this]() {
            qDebug() << "RESULT" << m_job->error() << m_job->errorString() << m_data.isNull();
            Q_EMIT changed();
        });
        connect(m_job, &KIO::PreviewJob::gotPreview, this, [this](const KFileItem &item, const QPixmap &preview) {
            qDebug() << "DATA" << item.url() << preview.isNull();
            m_data = preview;
            Q_EMIT changed();
        });

        // If we can't make a preview in 1s then this file can bugger right off.
        m_timer.setInterval(1s);
        connect(&m_timer, &QTimer::timeout, this, [this] {
            maybeKill();
            m_job = nullptr;
        });

        m_timer.start();
        m_job->start();
    }
    Q_SIGNAL void urlChanged();

    bool hasData()
    {
        return !m_data.isNull();
    }

    Q_SIGNAL void changed();

private:
    void maybeKill()
    {
        if (m_job) {
            m_job->kill();
        }
    }

    QPixmap m_data;
    QUrl m_url;
    QTimer m_timer;
    QPointer<KIO::PreviewJob> m_job;

    Q_DISABLE_COPY_MOVE(Preview); // rule of 5
};

class Clipboard : public QObject
{
    Q_OBJECT
public:
    using QObject::QObject;

    // Translates to cut=bool for KIO.
    enum class Transfer {
        Cut = true,
        Copy = false,
    };

    QList<QUrl> static localUrlsOf(const KFileItem &item)
    {
        bool local = false;
        QList<QUrl> urls {item.mostLocalUrl(&local)};
        if (!local) {
            return {};
        }
        return urls;
    }

    void static clip(const KFileItem &item, Transfer cut)
    {
        qDebug() << "clip" << item << static_cast<bool>(cut);
        const QList<QUrl> urls {item.url()};
        const QList<QUrl> mostLocalUrls = localUrlsOf(item);

        auto mime = new QMimeData;
        KIO::setClipboardDataCut(mime, static_cast<bool>(cut));
        KUrlMimeData::setUrls(urls, mostLocalUrls, mime);
        qGuiApp->clipboard()->setMimeData(mime);
    }

    Q_INVOKABLE static void copy(const KFileItem &item)
    {
        clip(item, Transfer::Copy);
    }

    Q_INVOKABLE static void cut(const KFileItem &item)
    {
        clip(item, Transfer::Cut);
    }

    Q_INVOKABLE static void paste(const QUrl &destDirUrl)
    {
        KIO::paste(QApplication::clipboard()->mimeData(), destDirUrl);
        // TODO: do we need to keep the job for anything?
        // TODO: do we need to set anything up vis a vis UI? pasting can have clashing file names and what not
    }
};

// Cheeky wrapper. Unsure how to best map KPropertiesDialog yet. It's a QDialog.
class PropertiesDialogFactory : public QObject
{
    Q_OBJECT
public:
    using QObject::QObject;

    Q_INVOKABLE static void show(const KFileItem &item)
    {
        // FIXME Lack of parent messes with transientness
        auto dialog = new KPropertiesDialog(item);
        dialog->show();
    }
};

class FileItem : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString iconName READ iconName CONSTANT)
public:
    explicit FileItem(KFileItem &item, QObject *parent = nullptr)
        : QObject(parent)
        , m_item(std::move(item))
    {
    }

    QString iconName() const
    {
        return m_item.iconName();
    }

private:
    const KFileItem m_item;
};

class PlacesModel : public KFilePlacesModel
{
    Q_OBJECT

    // TODO: make base enums Q_ENUM so we can reflect on them

public:
    enum class Role {
        GroupName = Qt::UserRole + 1,
    };
    Q_ENUM(Role);

    using KFilePlacesModel::KFilePlacesModel;

    QHash<int, QByteArray> roleNames() const override
    {
        auto roles = KFilePlacesModel::roleNames();

        roles.insert(UrlRole, "UrlRole");
        roles.insert(IconNameRole, "IconNameRole");

        for (int i = 0; i < m_roles.keyCount(); ++i) {
            roles.insert(m_roles.value(i), m_roles.key(i));
        }

        return roles;
    }

    QVariant data(const QModelIndex &index, int intRole = Qt::DisplayRole) const override
    {
        if (m_roles.valueToKey(intRole) == nullptr) {
            auto x = KFilePlacesModel::data(index, intRole);
            return x;
        }

        switch (static_cast<Role>(intRole)) {
        case Role::GroupName:
            switch (groupType(index)) {
            case PlacesType:
                return i18nc("@label", "Places");
            case RemoteType:
                return i18nc("@label", "Remote");
            case RecentlySavedType:
                return i18nc("@label", "Recently Saved");
            case SearchForType:
                return i18nc("@label", "Search");
            case DevicesType:
                return i18nc("@label", "Devices");
            case RemovableDevicesType:
                return i18nc("@label", "Removable Devices");
            case UnknownType:
                return i18nc("@label", "Unkown");
            case TagsType:
                return i18nc("@label", "Tags");
            }
        }

        return {};
    }

    Q_INVOKABLE bool move(int fromRow, int toRow)
    {
        qDebug() << fromRow << toRow;
        const bool ret = movePlace(fromRow, toRow);
        // TODO: sync should probably trigger after a while with the mouse nowhere near the listview?
        metaObject()->invokeMethod(this, &KFilePlacesModel::refresh); // sync to disk
        return ret;
    }

private:
    const QMetaEnum m_roles = QMetaEnum::fromType<Role>();
};

class DirModel : public KDirModel
{
    Q_OBJECT
    Q_PROPERTY(QUrl dir READ dir WRITE setDir NOTIFY dirChanged)
public:
    enum class Role {
        // KDirModel claims super high values. We should be save starting at UserRole. There's nary a chance we'll get
        // high enough.
        FileItem = Qt::UserRole + 1,
        IconName,
        HasChildren,
        Url,
        Directory,
    };
    Q_ENUM(Role);

    using KDirModel::KDirModel;

    QHash<int, QByteArray> roleNames() const override
    {
        auto roles = KDirModel::roleNames();

        roles[FileItemRole] = "FileItemRole";

        for (int i = 0; i < m_roles.keyCount(); ++i) {
            roles.insert(m_roles.value(i), m_roles.key(i));
        }

        return roles;
    }

    QVariant data(const QModelIndex &index, int intRole = Qt::DisplayRole) const override
    {
        if (!index.isValid() || index.row() < 0) {
            return {};
        }

        if (m_roles.valueToKey(intRole) == nullptr) {
            return KDirModel::data(index, intRole);
        }


        switch (static_cast<Role>(intRole)) {
            // FIXME: not handling fileitem
        case Role::IconName:
            return itemForIndex(index).iconName();
        case Role::HasChildren:
            return hasChildren(index);
        case Role::Url:
            return itemForIndex(index).url();
        case Role::Directory:
            return itemForIndex(index).isDir();
        }

        return {};
    }

    QUrl dir() const
    {
        return dirLister()->url();
    }
    void setDir(const QUrl &dir)
    {
        qDebug() << "opening" << dir;
        openUrl(dir);
        Q_EMIT dirChanged();
    }
    Q_SIGNAL void dirChanged();

private:
    const QMetaEnum m_roles = QMetaEnum::fromType<Role>();
};

class DirSortFilterProxyModel : public KDirSortFilterProxyModel
{
    using KDirSortFilterProxyModel::KDirSortFilterProxyModel;
};

int main(int argc, char **argv)
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);

    QApplication app(argc, argv);
    // app.setWindowIcon(QIcon::fromTheme(QStringLiteral("((TBD))")));
    app.setDesktopFileName(QStringLiteral("org.kde.hippo"));

    QQmlApplicationEngine engineObj;
    auto engine = &engineObj;

    auto l10nContext = new KLocalizedContext(engine);
    l10nContext->setTranslationDomain(QStringLiteral(TRANSLATION_DOMAIN));
    engine->rootContext()->setContextObject(l10nContext);
    KDeclarative::KDeclarative::setupEngine(engine);

    Clipboard clipboard;
    qmlRegisterSingletonInstance("org.kde.hippo", 1, 0, "Clipboard", &clipboard);

    PropertiesDialogFactory propertiesDialogFactory;
    qmlRegisterSingletonInstance("org.kde.hippo", 1, 0, "PropertiesDialogFactory", &propertiesDialogFactory);

    qmlRegisterType<Preview>("org.kde.hippo", 1, 0, "Preview");
    qmlRegisterType<PlacesModel>("org.kde.hippo", 1, 0, "PlacesModel");
    qmlRegisterType<DirModel>("org.kde.hippo", 1, 0, "DirModel");
    qmlRegisterType<DirSortFilterProxyModel>("org.kde.hippo", 1, 0, "DirSortFilterProxyModel");

    engine->load(QUrl(QStringLiteral("qrc:/ui/main.qml")));

    return app.exec();
}

#include "main.moc"
